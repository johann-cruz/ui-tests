*** Settings ***
Library  SeleniumLibrary

*** Variables ***
${search_result_label}  results for

*** Keywords ***
Verify Search Results
    
    [Arguments]             ${search_text}
    Page Should Contain     ${search_result_label}  ${search_text}
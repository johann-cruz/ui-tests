*** Settings ***
Library  SeleniumLibrary

***Variables***
${HomePageSearchTextBox}        xpath: //*[@id="gh-ac"]
${HomePageSearchButton}         xpath: //*[@id="gh-btn"]
${HomePageAdvancedSearchLink}   xpath: //*[@id="gh-as-a"]

*** Keywords ***
Input Search Text and Click Search
    [Arguments]     ${search_text}  
    Input Text      ${HomePageSearchTextBox}    ${search_text}
    Press Keys      ${HomePageSearchButton}     Return

Click on Advanced Search Link
    Click Element   ${HomePageAdvancedSearchLink}
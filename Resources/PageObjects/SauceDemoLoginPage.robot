*** Settings ***
Library  SeleniumLibrary

***Variables***
${LoginPageUserNameTextBox}     id: user-name
${LoginPagePasswordTextBox}     id: password
${LoginPageLoginButton}         xpath://*[@id="login-button"]
${LoginPageErrorMessageLabel}   xpath://*[@id="login_button_container"]/div/form/h3

***Keywords***
Invalid Login Scenarios
    [Arguments]     ${username}     ${password}     ${error_msg}
    Input Text      ${LoginPageUserNameTextBox}  ${username}
    Input Text      ${LoginPagePasswordTextBox}  ${password}
    Click button    ${LoginPageLoginButton} 
    Sleep   2s
    Element Should Contain   ${LoginPageErrorMessageLabel}   ${error_msg}
*** Settings ***
Library     SeleniumLibrary
Resource    ../../TestData/ConfigData.robot

*** Keywords ***
Start TestCase
    Open Browser    ${url.${env}}  ${browser}   options=add_argument("--ignore-certificate-errors")
    Maximize Browser Window

Finish TestCase
    Close Browser
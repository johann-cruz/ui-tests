***Settings***
Library     SeleniumLibrary
Library     DataDriver      ../TestData/TestData.csv
Resource    ../Resources/Commons/Utilities.robot
Resource    ../Resources/PageObjects/SauceDemoLoginPage.robot

Suite Setup         Utilities.Start TestCase
Suite Teardown      Utilities.Finish TestCase
Test Template       Invalid Login Scenarios
# TODO: For some reason, there is an error when using SauceDemoLoginPage.Invalid Login Scenarios in Test Template

***Variables***
${txtbox_username}  id:user-name
${txtbox_password}  id:password
${btn_login}        xpath://*[@id="login-button"]
${txt_error}        xpath://*[@id="login_button_container"]/div/form/h3

***Test Cases***
Verify Login Fails with Invalid Creds   ${username}     ${password}     ${error_msg}

***Keywords***
Invalid Login Scenarios
    [Arguments]                 ${username}         ${password}     ${error_msg}
    Input Text                  ${txtbox_username}  ${username}
    Input Text                  ${txtbox_password}  ${password}
    Click button                ${btn_login} 
    Sleep   2s
    Element Should Contain      ${txt_error}        ${error_msg}
*** Settings ***
Resource  ../Resources/Commons/Utilities.robot
Resource  ../Resources/PageObjects/EbayDemoHeaderPage.robot
Resource  ../Resources/PageObjects/EbayDemoSearchResultsPage.robot

Suite Setup      Utilities.Start TestCase
Suite Teardown   Utilities.Finish TestCase

*** Test Cases ***
Verify basic search functionality
    [Documentation]  This test case verifies the basic search
    [Tags]  Functional

    EbayDemoHeaderPage.Input Search Text and Click Search   robot
    EbayDemoSearchResultsPage.Verify Search Results         robot

Verify advanced search functionality
    [Documentation]  This test case verifies the advanced search
    [Tags]  Functional

    EbayDemoHeaderPage.Click on Advanced Search Link
***Settings***
Library     SeleniumLibrary
Resource    ../Resources/Commons/Utilities.robot
Resource    ../Resources/PageObjects/SauceDemoLoginPage.robot

Suite Setup         Utilities.Start TestCase
Suite Teardown      Utilities.Finish TestCase
Test Template       SauceDemoLoginPage.Invalid Login Scenarios

***Test Cases***                                        USERNAME            PASSWORD            ERROR MESSAGE
Verify Login Fails - Blank Username and Password        ${EMPTY}            ${EMPTY}            Epic sadface: Username is required
Verify Login Fails - Wrong Username                     standard_us         secret_sauce        Epic sadface: Username and password do not match any user in this service
Verify Login Fails - LockedOut User                     locked_out_user     secret_sauce        Epic sadface: Sorry, this user has been locked out.
Verify Login Fails - Wrong Password                     standard_user       secret_sa           Epic sadface: Username and password do not match any user in this service     
Verify Login Fails - Wrong Username and Password        standard_use        secret_sa           Epic sadface: Username and password do not match any user in this service